# Phreakuency Radio

## The Pirate Radio
Free Underground Online Internet Radio. https://phreakuency.net/

## Backend and ansible
Here is how the radio works: \
https://git.puscii.nl/totalreset/ansible-roles/-/tree/main/roles/phreakuency_radio
This is the role to push music to the server and update the radio: \
https://git.puscii.nl/totalreset/ansible-roles/-/tree/main/roles/music_to_phreakuency


## This page
This is the old website with its files. New website in progress.
