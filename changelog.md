# Changelog

## v1.2
- November 2021. Fixed id3 with correct verions (1).

## v1.1 
- November 2021. Fixed id3 tags and "Now playing" metadata js.

## v1.0 
- October 2021. "Now playing" option released, now showing the metadata of the songs.

## v0.7

- 8th January 2021. Made a better page for "The Archive" and done a graphic for it. New domain aswell "thearchive.phreakuency.tk". 

## v0.6

- 1 January 2021. First sets updated on "The Archive".

## v0.3

- 14th Dicember 2020. Added ffmpeg script that takes audio from video streamed sended via RTMP and send it into liquidsoap. Live programs are working in audio only aswell.

## v0.2

- 27th November 2020. Video Live player added, live shows soon. Font color changes at refresh. "The Archive" added. New domains now! phreakuency.tk

## v0.1

- 10th October 2020. Graphics from Alix added, video streaming support and css modified

## v0.03

- 13th September. 24/7 Auto dj script with smart fading. Jingles are added and difference between day and night playlists.

## v0.02

- 16th May. Website created and support to multiple inputs/outputs with icecast.

## v0.01

- Somewhen in spring 2020. beginning of corona. First website and streaming server set up with icecast2. Realese party happened streaming Mark's bedroom party.
